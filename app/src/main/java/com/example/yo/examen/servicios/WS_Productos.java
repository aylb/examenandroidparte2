package com.example.yo.examen.servicios;

import android.os.StrictMode;

import com.example.yo.examen.util.Constantes;
import com.example.yo.examen.ws.ResProducto;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;




public class WS_Productos {

    public ResProducto resProducto(){

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        ResProducto resultado = new ResProducto();
        URL url = null;
        HttpsURLConnection conn;
        BufferedReader in;
        String inputLine = "";
        StringBuffer response;
        String json = "";

        try {
            url = new URL(Constantes.WMT);
            conn = (HttpsURLConnection) url.openConnection();
            conn.setRequestMethod(Constantes.GET);

            conn.setRequestProperty(Constantes.ACCEPT, Constantes.APP_JSON);
            conn.setRequestProperty(Constantes.CONNECTION, Constantes.KEEP_ALIVE);
            conn.setRequestProperty(Constantes.CONTENT_TYPE, Constantes.APP_JSON);
            conn.setRequestProperty(Constantes.COOKIE, Constantes.COOKIE_VAL);
            conn.connect();

            in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            response = new StringBuffer();


            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }

            json = response.toString();

            JSONObject jsonObject = new JSONObject(json);

            resultado.setSkuDisplayNameText(jsonObject.getString(Constantes.SKU_DISP_NAME));
            resultado.setDepartment(jsonObject.getString(Constantes.DEPARTMENT));
            resultado.setSkuId(jsonObject.getString(Constantes.SKU_ID));
            resultado.setBasePrice(jsonObject.getString(Constantes.BASE_PRICE));


        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return resultado;

    }



}

