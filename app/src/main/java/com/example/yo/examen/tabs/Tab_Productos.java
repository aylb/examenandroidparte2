package com.example.yo.examen.tabs;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import com.example.yo.examen.R;

import com.example.yo.examen.bd.DataBaseReg;
import com.example.yo.examen.servicios.WS_Productos;
import com.example.yo.examen.ws.ResProducto;

public class Tab_Productos extends Activity {

    String nPersona = "";

    ResProducto resultado;
    WS_Productos obtiene;

    DataBaseReg db;

    TextView txtV_skuName;
    TextView txtV_depart;
    TextView txtV_skuId;
    TextView txtV_bPrice;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.informacion_producto);
        txtV_skuName  = (TextView) findViewById(R.id.txtSkuDisplay);
        txtV_depart = (TextView) findViewById(R.id.department);
        txtV_skuId = (TextView) findViewById(R.id.skuId);
        txtV_bPrice = (TextView) findViewById(R.id.basePrice);

        db = new DataBaseReg( getApplicationContext() );
        int id= 1;

        Bundle saludo = this.getIntent().getExtras();

        nPersona= saludo.getString("SALUDO") + db.obtener(id);

        Toast toast1 = Toast.makeText(getApplicationContext(), nPersona, Toast.LENGTH_SHORT);

        obtiene = new WS_Productos();
        resultado = new ResProducto();

        resultado = obtiene.resProducto();

        txtV_skuName.setText(resultado.getSkuDisplayNameText());
        txtV_depart.setText(resultado.getDepartment());
        txtV_skuId.setText(resultado.getSkuId());
        txtV_bPrice.setText("$" + resultado.getBasePrice());

        toast1.show();

    }

}
