package com.example.yo.examen.util;

public class Constantes {

    //HTTP Metodos
    public static String GET = "GET";

    //URLs
    public static final String WMT = "https://super.walmart.com.mx/api/rest/model/atg/commerce/catalog/ProductCatalogActor/getSkuSummaryDetails?storeId=0000009999&upc=00750129560012&skuId=00750129560012";

    //Headers
    public static final String ACCEPT = "Accept";
    public static final String CONNECTION = "Connection";
    public static final String CONTENT_TYPE = "Content-Type";
    public static final String COOKIE = "Cookie";

    //Valores Headers

    public static final String APP_JSON = "application/json; charset=utf-8";
    public static final String KEEP_ALIVE = "keep-alive";
    public static final String COOKIE_VAL = "JSESSIONID_GR=ewnljk%2BfgPUYt2Uks5PY-vG9.restapp-298183240-6-359372925; TS01f4281b=0130aff232b466ffcc4072d1bbce44ecbbd89f5d479ff3da4fb92ddaa94160888e2ca908d1fa72efd98d848e3e8531b6c0d47a99fe; akavpau_vp_super=1544643414~id%3D0b950c261050bdafe78b05c390a1fd75; dtCookie=%7CbWV4aWNvX19ncm9jZXJpZXN8MA; TS01c7b722=0130aff232b466ffcc4072d1bbce44ecbbd89f5d479ff3da4fb92ddaa94160888e2ca908d1fa72efd98d848e3e8531b6c0d47a99fe";


    //JSON
    public static final String SKU_DISP_NAME = "skuDisplayNameText";
    public static final String DEPARTMENT = "department";
    public static final String SKU_ID = "skuId";
    public static final String BASE_PRICE = "basePrice";




}
